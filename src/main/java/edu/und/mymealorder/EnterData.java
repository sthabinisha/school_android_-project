package edu.und.mymealorder;

import androidx.appcompat.app.AppCompatActivity;
import edu.und.mymealorder.adapter.MealAdapter;
import edu.und.mymealorder.database.MySQliteHelper;
import edu.und.mymealorder.database.DatabaseInfo;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class EnterData extends AppCompatActivity {

    Button enterData;
    EditText foodName;
    EditText foodPrice;
    private MySQliteHelper db;
    MainActivity mainActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_data);
        enterData = findViewById(R.id.enterMenu);
        foodName = findViewById(R.id.itemName);
        foodPrice = findViewById(R.id.itemPrice);
        final DatabaseInfo databaseInfo = new DatabaseInfo();
        db = new MySQliteHelper(this);


        enterData.setOnClickListener(new View.OnClickListener() {
            @Override
            public int hashCode() {
                return super.hashCode();
            }

            @Override
            public void onClick(View view) {
                if (foodName.getText().toString().isEmpty() || foodPrice.getText().toString().isEmpty()){
                    Toast.makeText(getApplicationContext(), "Enter the Data", Toast.LENGTH_SHORT).show();

                }else{
                    MySQliteHelper helper = new MySQliteHelper(getApplicationContext());
                    helper.insertMenu(foodName.getText().toString(), foodPrice.getText().toString());


                    Toast.makeText(getApplicationContext(), "Inserted", Toast.LENGTH_SHORT).show();




                }
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent enterDataIntent = new Intent(EnterData.this, MainActivity.class);
         startActivity(enterDataIntent);
    }

    @Override
    public void finish() {
        Intent returnIntent = new Intent();
        returnIntent.putExtra("passed_item", "0");
        // setResult(RESULT_OK);
        setResult(RESULT_OK, returnIntent); //By not passing the intent in the result, the calling activity will get null data.
        super.finish();
    }
}
