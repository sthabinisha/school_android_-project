package edu.und.mymealorder.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.ContactsContract;
import android.widget.ArrayAdapter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import edu.und.mymealorder.adapter.MealAdapter;

public class MySQliteHelper extends SQLiteOpenHelper {

    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "food_order.db";


    ArrayList<DatabaseInfo> comboList = new ArrayList<>();
    String combo_treat, comboNoComma;
    float sum = 0;
    public MySQliteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {

        // create food table
        db.execSQL(DatabaseInfo.CREATE_FOOD_TABLE);
        // create combo table
        db.execSQL(DatabaseInfo.CREATE_MEAL_TABLE);
        db.execSQL(DatabaseInfo.CREATE_FOOD_COMBO_TABLE);


    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + DatabaseInfo.FOOD_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + DatabaseInfo.COMBO_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + DatabaseInfo.FOOD_CONN);


        // Create tables again
        onCreate(db);
    }

    public boolean insertMenu(String foodname, String price) {
        // get writable database as we want to write data
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(DatabaseInfo.UNIQUE_NAME, foodname);
        values.put(DatabaseInfo.PRICE, price);


        // insert row
        db.insert(DatabaseInfo.FOOD_TABLE, null, values);

        // close db connection
        db.close();

        // return newly inserted row id
        return true;
    }

    public void insertComboMeal(ArrayList<DatabaseInfo> foonameSelected) {
        this.comboList = foonameSelected;
        // get writable database as we want to write data
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        StringBuilder sbString = new StringBuilder("");

        // `id` and `timestamp` will be inserted automatically.
        // no need to add them
        for(int i= 0; i<comboList.size(); i++){
            sbString.append(foonameSelected.get(i).getUniqueName()).append(",");
            sum+= foonameSelected.get(i).getPRICE();
        }
        combo_treat= sbString.toString();


        combo_treat = combo_treat.substring(0, combo_treat.length() - 1);

        values.put(DatabaseInfo.COMBO,combo_treat);
        values.put(DatabaseInfo.PRICE, sum);



        // insert row
        db.insert(DatabaseInfo.COMBO_TABLE, null, values);

        String selectFromComboQuery = "SELECT "+ DatabaseInfo.UNIQUE_ID + " FROM " + DatabaseInfo.COMBO_TABLE +
                "  where " + DatabaseInfo.COMBO+ "='" + combo_treat+"'";
        Cursor cursor = db.rawQuery(selectFromComboQuery, null);
        if(cursor.moveToFirst()){
            do {
               int unique_id = cursor.getInt(cursor.getColumnIndex(DatabaseInfo.UNIQUE_ID));
               insertConTable(foonameSelected, unique_id);
            } while (cursor.moveToNext());

        }

        // close db connection
        db.close();
        //insertConTable(id, foodname);


        // return newly inserted row id
        //return true;
    }

    public boolean  insertConTable(ArrayList<DatabaseInfo> foonameSelected, int unique_id) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        this.comboList = foonameSelected;
        for(int i= 0; i<comboList.size(); i++) {
            values.put(DatabaseInfo.UNIQUE_ID, unique_id);
            values.put(DatabaseInfo.UNIQUE_NAME, foonameSelected.get(i).getUniqueName());
//            String selectFromFoodQuery = "SELECT "+ DatabaseInfo.UNIQUE_NAME +" FROM " + DatabaseInfo.FOOD_TABLE
//                    + " where " + DatabaseInfo.UNIQUE_NAME + "=" + foonameSelected.get(i).getUniqueName();
//            Cursor cursor = db.rawQuery(selectFromFoodQuery, null);
//            if(cursor.moveToFirst()){
//                do {
//                    values.put(DatabaseInfo.UNIQUE_ID, unique_id);
//                    values.put(DatabaseInfo.UNIQUE_NAME, cursor.getString(cursor.getColumnIndex(DatabaseInfo.UNIQUE_NAME)));
//                } while (cursor.moveToNext());
//
//            }

            // `id` and `timestamp` will be inserted automatically.
            // no need to add them

        }




        // insert row
        db.insert(DatabaseInfo.FOOD_CONN, null, values);

        // close db connection
        db.close();


        // return newly inserted row id
        return true;
    }

    public List<DatabaseInfo> getMenu() {
        List<DatabaseInfo> menu = new ArrayList<>();

        // Select All Query
        String selectQuery = "SELECT  * FROM " + DatabaseInfo.FOOD_TABLE + " ORDER BY " +
                DatabaseInfo.UNIQUE_NAME + " DESC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                DatabaseInfo info = new DatabaseInfo();
                info.setUnique_name(cursor.getString(cursor.getColumnIndex(DatabaseInfo.UNIQUE_NAME)));
                info.setPrice(cursor.getFloat(cursor.getColumnIndex(DatabaseInfo.PRICE)));

                menu.add(info);
            } while (cursor.moveToNext());
        }
        // close db connection
        db.close();

        // return menu list
        return menu;
    }
    public List<DatabaseInfo> getCombo() {
        List<DatabaseInfo> menu = new ArrayList<>();

        // Select All Query
        String selectQuery = "SELECT  * FROM " + DatabaseInfo.COMBO_TABLE + " ORDER BY " +
                DatabaseInfo.UNIQUE_ID + " DESC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                DatabaseInfo info = new DatabaseInfo();
                info.setCombo(cursor.getString(cursor.getColumnIndex(DatabaseInfo.COMBO)));
                info.setPrice(cursor.getFloat(cursor.getColumnIndex(DatabaseInfo.PRICE)));

                menu.add(info);
            } while (cursor.moveToNext());
        }
        // close db connection
        db.close();

        // return menu list
        return menu;
    }
    public int UpdataPrice(Float update_price, String food_name){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        //values.put();
        return 1;
    }

    public void updateSelected(ArrayList<DatabaseInfo> databaseInfos) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        for (int i = 0; i < databaseInfos.size(); i++) {
//            System.out.println( databaseInfos.get(i).getEditTextValue().toString());
            //values.put(DatabaseInfo.COMBO, comboList.get(i).getCOMBO());
            values.put(DatabaseInfo.PRICE, databaseInfos.get(i).getEditTextValue());
            db.update(DatabaseInfo.FOOD_TABLE, values, DatabaseInfo.UNIQUE_NAME + " = ?",
                    new String[]{String.valueOf(databaseInfos.get(i).getUniqueName())});


        }
    }

    public boolean deleteCombo(String s) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(DatabaseInfo.COMBO_TABLE, DatabaseInfo.COMBO + " = ?",
                new String[]{s});

        db.close();
        return true;

    }

    public int getComboCount() {
        String countQuery = "SELECT  * FROM " + DatabaseInfo.COMBO_TABLE;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);

        int count = cursor.getCount();
        cursor.close();


        // return count
        return count;
    }

}