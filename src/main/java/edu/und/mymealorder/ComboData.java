package edu.und.mymealorder;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import edu.und.mymealorder.adapter.ComboAdapter;
import edu.und.mymealorder.database.MySQliteHelper;
import edu.und.mymealorder.database.DatabaseInfo;

public class ComboData extends AppCompatActivity {
    Button enterData;
    private MySQliteHelper db;
    private ComboAdapter comboAdapter;
    RecyclerView recyclerView;
    Button button_combo;
    List<DatabaseInfo> food_list = new ArrayList<DatabaseInfo>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_combo_data);
        db = new MySQliteHelper(this);
        recyclerView = findViewById(R.id.recycler_view);
        button_combo = findViewById(R.id.Delete);

        food_list.addAll(db.getCombo());
        comboAdapter = new ComboAdapter(this, food_list);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(comboAdapter);
        comboAdapter.notifyDataSetChanged();
        button_combo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MySQliteHelper helpers = new MySQliteHelper(getApplicationContext());
                DatabaseInfo databaseInfo;
                int position =( (ComboAdapter)recyclerView.getAdapter()).getSelected();

                helpers.deleteCombo(food_list.get(position).getCOMBO());
                food_list.remove(position);
                comboAdapter.notifyItemRemoved(position);

                comboAdapter.notifyItemRangeChanged(position, db.getComboCount());

            }
        });




    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent enterDataIntent = new Intent(ComboData.this, MainActivity.class);
        startActivity(enterDataIntent);
    }
}
