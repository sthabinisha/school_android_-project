package edu.und.mymealorder;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import edu.und.mymealorder.adapter.MealAdapter;
import edu.und.mymealorder.database.MySQliteHelper;
import edu.und.mymealorder.database.DatabaseInfo;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity {

    Button enterData;
    private MySQliteHelper db;
    private MealAdapter mealAdapter;
    RecyclerView recyclerView;
    Button button_combo;
    Button button_insert;
    Button button_update;
    Button button_como;
    List<DatabaseInfo> food_list = new ArrayList<edu.und.mymealorder.database.DatabaseInfo>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        db = new MySQliteHelper(this);
        recyclerView = findViewById(R.id.recycler_view);
        button_combo = findViewById(R.id.addCombo);
        button_insert = findViewById(R.id.addData);
        button_update = findViewById(R.id.updatePrice);
        button_como = findViewById(R.id.Combo);

        food_list.addAll(db.getMenu());
        mealAdapter = new MealAdapter(this, food_list);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mealAdapter);
            mealAdapter.notifyDataSetChanged();
            button_insert.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent enterDataIntent = new Intent(MainActivity.this, EnterData.class);
                                startActivity(enterDataIntent);

                }
            });
            button_combo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MySQliteHelper helper = new MySQliteHelper(getApplicationContext());
                    ArrayList<DatabaseInfo> databaseInfos =( (MealAdapter)recyclerView.getAdapter()).getSelectedList();

                    helper.insertComboMeal(databaseInfos);

                    System.out.println(databaseInfos.size());
                    mealAdapter.notifyDataSetChanged();
                    Intent intent = new Intent(MainActivity.this, ComboData.class);
                    startActivity(intent);
                }
            });

            button_update.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MySQliteHelper helper = new MySQliteHelper(getApplicationContext());
//
                                ArrayList<DatabaseInfo> databaseInfos =( (MealAdapter)recyclerView.getAdapter()).getSelectedList();
                                helper.updateSelected(databaseInfos);

                    Intent intent= getIntent();
                    finish();
                    startActivity(intent);
                                Toast.makeText(MainActivity.this, "Updated", Toast.LENGTH_SHORT).show();
                }
            });

            button_como.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(MainActivity.this, ComboData.class);
                    startActivity(intent);

                }
            });


    }


}
