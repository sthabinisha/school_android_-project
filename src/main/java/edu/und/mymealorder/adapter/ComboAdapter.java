package edu.und.mymealorder.adapter;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import edu.und.mymealorder.ComboData;
import edu.und.mymealorder.MainActivity;
import edu.und.mymealorder.R;
import edu.und.mymealorder.database.DatabaseInfo;

public class ComboAdapter extends RecyclerView.Adapter<ComboAdapter.MyViewHolder> {
    private List<DatabaseInfo> getDataList;
    ArrayList<DatabaseInfo> combo_list = new ArrayList<>();
    private static RadioButton lastChecked = null;
    private static int lastCheckedPos = 0;

    Context context;

    public ComboAdapter(ComboData comboData, List<DatabaseInfo> food_list) {
        this.context = comboData;
        this.getDataList = food_list;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView food;
        public TextView price;

        public RadioButton radioButton;

        public MyViewHolder(View view) {
            super(view);
            food = view.findViewById(R.id.foodname);
            price = view.findViewById(R.id.foodPrice);
            radioButton = view.findViewById(R.id.radio_food);
        }
    }

    @Override
    public ComboAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.combo_single_item, parent, false);

        return new ComboAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ComboAdapter.MyViewHolder holder, final int position) {
        final DatabaseInfo databaseInfo = getDataList.get(position);
        holder.food.setText(databaseInfo.getCOMBO());
        holder.price.setText(databaseInfo.getPRICE().toString());
        holder.radioButton.setChecked(databaseInfo.isSelected());
        holder.radioButton.setTag(new Integer(position));


        holder.radioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RadioButton radioButton = (RadioButton)view;
                int ClickedPos = ((Integer)radioButton.getTag()).intValue();
                if(radioButton.isChecked()){
                    if(lastChecked!= null){
                        lastChecked.setChecked(false);
                        databaseInfo.setSelected(true);
                    }
                    lastChecked = radioButton;
                    lastCheckedPos = ClickedPos;
                }else{
                    lastChecked = null;
                    databaseInfo.setSelected(true);
                }
            }
        });

    }

    public  int getSelected(){
        int selected_combo;


        return lastCheckedPos;
    }

    @Override
    public int getItemCount() {
        return getDataList.size();
    }

    public ArrayList<DatabaseInfo> getSelectedList() {
        combo_list.clear();

        for (int i = 0; i < getDataList.size(); i++) {
            if (getDataList.get(i).isSelected()) {
                combo_list.add(getDataList.get(i));
            }else{
                combo_list.remove(getDataList.get(i));
            }

        }
        return combo_list;
    }
}
